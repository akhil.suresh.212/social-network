var express = require("express");
const JWT = require("../utils/jwt");
var router = express.Router();
const { FRIENDS_TABLE, USER_TABLE } = require("../constants/tableNames");
const knex = require("../db");
const { errorResponse, successResponse } = require("../utils/response");
jwt = new JWT();

router.post("/add", jwt.authenticateToken, (req, res) => {
  const {
    body: { friend, user },
  } = req;

  console.log({ friend, user });

  if (!friend) errorResponse(res, "missing required fields");

  knex(FRIENDS_TABLE)
    .where({ user_token: user.userToken, friend_token: friend })
    .count("* as count")
    .then((count) => {
      if (count[0].count != 0) {
        errorResponse(res, "already exists");
        return;
      }
      knex(FRIENDS_TABLE)
        .insert({
          user_token: user.userToken,
          friend_token: friend,
        })
        .then((doc) => {
          console.log({ doc });
          successResponse(res, doc);
        })
        .catch((err) => {
          errorResponse(res, "Something went wrong", 500, err);
        })
        .catch((err) => {
          errorResponse(res, "Something went wrong", 500, err);
        });
    });
});

router.get("/requests", jwt.authenticateToken, (req, res) => {
  knex(FRIENDS_TABLE)
    .where({ user_token: req.body.user.userToken, friend_status: "request" })
    .leftJoin(
      USER_TABLE,
      USER_TABLE + ".token",
      FRIENDS_TABLE + ".friend_token"
    )
    .select(
      FRIENDS_TABLE + ".friend_id",
      USER_TABLE + ".full_name",
      USER_TABLE + ".token",
      USER_TABLE + ".email_id",
      USER_TABLE + ".p_pic"
    )
    .then((doc) => {
      successResponse(res, doc);
    })
    .catch((err) => {
      errorResponse(res, "Something went wrong", err);
    });
});

router.post("/accept", jwt.authenticateToken, (req, res) => {
  const {
    body: { request_id, user },
  } = req;

  if (!request_id) errorResponse(res, "invalid parameters");

  knex(FRIENDS_TABLE)
    .update({ friend_status: 2 })
    .then((doc) => {
      if (doc) {
        successResponse(res, {});
      } else {
        errorResponse(res, "something went wrong");
      }
    })
    .catch((err) => {
      errorResponse(res, "something went wrong", err);
    });
});

router.get("/", jwt.authenticateToken, (req, res) => {
  knex(FRIENDS_TABLE)
    .where({
      user_token: req.body.user.userToken,
      friend_status: "accepted",
    })
    .leftJoin(
      USER_TABLE,
      USER_TABLE + ".token",
      FRIENDS_TABLE + ".friend_token"
    )
    .select(
      USER_TABLE + ".full_name",
      USER_TABLE + ".token",
      USER_TABLE + ".email_id",
      USER_TABLE + ".p_pic"
    )
    .then((doc) => {
      successResponse(res, doc);
    })
    .catch((err) => {
      errorResponse(res, "Something went wrong", err);
    });
});

module.exports = router;

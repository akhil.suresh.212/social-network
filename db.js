const dotenv = require("dotenv");
dotenv.config();

const {
  env: { db_host, db_user, db_pass, db_database },
} = process;

const knex = require("knex")({
  client: "mysql",
  connection: {
    host: db_host,
    user: db_user,
    password: db_pass,
    database: db_database,
  },
});

module.exports = knex;
